<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220909231531 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cliente DROP CONSTRAINT fk_f41c9b257475e0c0');
        $this->addSql('DROP SEQUENCE data_empresa_id_seq CASCADE');
        $this->addSql('ALTER TABLE data_empresa DROP CONSTRAINT fk_dcdc0ca1cc5858f9');
        $this->addSql('DROP TABLE data_empresa');
        $this->addSql('DROP INDEX uniq_f41c9b257475e0c0');
        $this->addSql('ALTER TABLE cliente DROP id_data_empresa_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE data_empresa_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE data_empresa (id INT NOT NULL, id_representante_id INT DEFAULT NULL, nombre_empresa VARCHAR(150) NOT NULL, cantidad_empleados INT DEFAULT NULL, cantidad_sucursales INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_dcdc0ca1cc5858f9 ON data_empresa (id_representante_id)');
        $this->addSql('ALTER TABLE data_empresa ADD CONSTRAINT fk_dcdc0ca1cc5858f9 FOREIGN KEY (id_representante_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cliente ADD id_data_empresa_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cliente ADD CONSTRAINT fk_f41c9b257475e0c0 FOREIGN KEY (id_data_empresa_id) REFERENCES data_empresa (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_f41c9b257475e0c0 ON cliente (id_data_empresa_id)');
    }
}
