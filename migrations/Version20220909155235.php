<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220909155235 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE documentos_persona ADD cliente_id INT NOT NULL');
        $this->addSql('ALTER TABLE documentos_persona ADD CONSTRAINT FK_1563C9A8DE734E51 FOREIGN KEY (cliente_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1563C9A8DE734E51 ON documentos_persona (cliente_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE documentos_persona DROP CONSTRAINT FK_1563C9A8DE734E51');
        $this->addSql('DROP INDEX IDX_1563C9A8DE734E51');
        $this->addSql('ALTER TABLE documentos_persona DROP cliente_id');
    }
}
