<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220908095557 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cliente DROP CONSTRAINT fk_f41c9b25aaf41547');
        $this->addSql('DROP INDEX uniq_f41c9b25aaf41547');
        $this->addSql('ALTER TABLE cliente RENAME COLUMN id_representante_legal_id TO id_data_empresa_id');
        $this->addSql('ALTER TABLE cliente ADD CONSTRAINT FK_F41C9B257475E0C0 FOREIGN KEY (id_data_empresa_id) REFERENCES data_empresa (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F41C9B257475E0C0 ON cliente (id_data_empresa_id)');
        $this->addSql('ALTER TABLE documentos_persona ADD id_representante_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE documentos_persona ADD CONSTRAINT FK_1563C9A8CC5858F9 FOREIGN KEY (id_representante_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1563C9A8CC5858F9 ON documentos_persona (id_representante_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE documentos_persona DROP CONSTRAINT FK_1563C9A8CC5858F9');
        $this->addSql('DROP INDEX UNIQ_1563C9A8CC5858F9');
        $this->addSql('ALTER TABLE documentos_persona DROP id_representante_id');
        $this->addSql('ALTER TABLE cliente DROP CONSTRAINT FK_F41C9B257475E0C0');
        $this->addSql('DROP INDEX UNIQ_F41C9B257475E0C0');
        $this->addSql('ALTER TABLE cliente RENAME COLUMN id_data_empresa_id TO id_representante_legal_id');
        $this->addSql('ALTER TABLE cliente ADD CONSTRAINT fk_f41c9b25aaf41547 FOREIGN KEY (id_representante_legal_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_f41c9b25aaf41547 ON cliente (id_representante_legal_id)');
    }
}
