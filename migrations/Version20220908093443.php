<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220908093443 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE cliente_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE correo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE data_empresa_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE direccion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE documento_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE documentos_persona_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE sexo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE cliente (id INT NOT NULL, id_representante_legal_id INT DEFAULT NULL, id_sexo_id INT DEFAULT NULL, edad INT DEFAULT NULL, primer_nombre VARCHAR(35) NOT NULL, segundo_nombre VARCHAR(35) DEFAULT NULL, primer_apellido VARCHAR(35) NOT NULL, segundo_apellido VARCHAR(35) DEFAULT NULL, fecha_nacimiento TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F41C9B25AAF41547 ON cliente (id_representante_legal_id)');
        $this->addSql('CREATE INDEX IDX_F41C9B25F5AF7228 ON cliente (id_sexo_id)');
        $this->addSql('CREATE TABLE correo (id INT NOT NULL, id_cliente_id INT NOT NULL, email VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_77040BC97BF9CE86 ON correo (id_cliente_id)');
        $this->addSql('CREATE TABLE data_empresa (id INT NOT NULL, nombre_empresa VARCHAR(150) NOT NULL, cantidad_empleados INT DEFAULT NULL, cantidad_sucursales INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE direccion (id INT NOT NULL, cliente_id INT NOT NULL, descripcion VARCHAR(600) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F384BE95DE734E51 ON direccion (cliente_id)');
        $this->addSql('CREATE TABLE documento (id INT NOT NULL, nombre VARCHAR(35) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE documentos_persona (id INT NOT NULL, id_documento_id INT NOT NULL, id_cliente_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1563C9A86601BA07 ON documentos_persona (id_documento_id)');
        $this->addSql('CREATE INDEX IDX_1563C9A87BF9CE86 ON documentos_persona (id_cliente_id)');
        $this->addSql('CREATE TABLE sexo (id INT NOT NULL, nombre VARCHAR(15) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE cliente ADD CONSTRAINT FK_F41C9B25AAF41547 FOREIGN KEY (id_representante_legal_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cliente ADD CONSTRAINT FK_F41C9B25F5AF7228 FOREIGN KEY (id_sexo_id) REFERENCES sexo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE correo ADD CONSTRAINT FK_77040BC97BF9CE86 FOREIGN KEY (id_cliente_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE direccion ADD CONSTRAINT FK_F384BE95DE734E51 FOREIGN KEY (cliente_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE documentos_persona ADD CONSTRAINT FK_1563C9A86601BA07 FOREIGN KEY (id_documento_id) REFERENCES documento (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE documentos_persona ADD CONSTRAINT FK_1563C9A87BF9CE86 FOREIGN KEY (id_cliente_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE cliente_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE correo_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE data_empresa_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE direccion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE documento_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE documentos_persona_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE sexo_id_seq CASCADE');
        $this->addSql('ALTER TABLE cliente DROP CONSTRAINT FK_F41C9B25AAF41547');
        $this->addSql('ALTER TABLE cliente DROP CONSTRAINT FK_F41C9B25F5AF7228');
        $this->addSql('ALTER TABLE correo DROP CONSTRAINT FK_77040BC97BF9CE86');
        $this->addSql('ALTER TABLE direccion DROP CONSTRAINT FK_F384BE95DE734E51');
        $this->addSql('ALTER TABLE documentos_persona DROP CONSTRAINT FK_1563C9A86601BA07');
        $this->addSql('ALTER TABLE documentos_persona DROP CONSTRAINT FK_1563C9A87BF9CE86');
        $this->addSql('DROP TABLE cliente');
        $this->addSql('DROP TABLE correo');
        $this->addSql('DROP TABLE data_empresa');
        $this->addSql('DROP TABLE direccion');
        $this->addSql('DROP TABLE documento');
        $this->addSql('DROP TABLE documentos_persona');
        $this->addSql('DROP TABLE sexo');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
