<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220908101057 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE data_empresa ADD id_representante_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE data_empresa ADD CONSTRAINT FK_DCDC0CA1CC5858F9 FOREIGN KEY (id_representante_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DCDC0CA1CC5858F9 ON data_empresa (id_representante_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE data_empresa DROP CONSTRAINT FK_DCDC0CA1CC5858F9');
        $this->addSql('DROP INDEX UNIQ_DCDC0CA1CC5858F9');
        $this->addSql('ALTER TABLE data_empresa DROP id_representante_id');
    }
}
