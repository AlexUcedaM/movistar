<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220908100317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE documentos_persona ADD id_representante_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE documentos_persona ADD CONSTRAINT FK_1563C9A8CC5858F9 FOREIGN KEY (id_representante_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1563C9A8CC5858F9 ON documentos_persona (id_representante_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE documentos_persona DROP CONSTRAINT FK_1563C9A8CC5858F9');
        $this->addSql('DROP INDEX UNIQ_1563C9A8CC5858F9');
        $this->addSql('ALTER TABLE documentos_persona DROP id_representante_id');
    }
}
