<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220908220322 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cliente ADD telefono VARCHAR(12) DEFAULT NULL');
        $this->addSql('ALTER TABLE cliente ADD celular VARCHAR(12) DEFAULT NULL');
        $this->addSql('ALTER TABLE cliente ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE correo ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE data_empresa ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE direccion ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE documento ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE documentos_persona ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE sexo ALTER id DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE data_empresa_id_seq');
        $this->addSql('SELECT setval(\'data_empresa_id_seq\', (SELECT MAX(id) FROM data_empresa))');
        $this->addSql('ALTER TABLE data_empresa ALTER id SET DEFAULT nextval(\'data_empresa_id_seq\')');
        $this->addSql('CREATE SEQUENCE sexo_id_seq');
        $this->addSql('SELECT setval(\'sexo_id_seq\', (SELECT MAX(id) FROM sexo))');
        $this->addSql('ALTER TABLE sexo ALTER id SET DEFAULT nextval(\'sexo_id_seq\')');
        $this->addSql('CREATE SEQUENCE correo_id_seq');
        $this->addSql('SELECT setval(\'correo_id_seq\', (SELECT MAX(id) FROM correo))');
        $this->addSql('ALTER TABLE correo ALTER id SET DEFAULT nextval(\'correo_id_seq\')');
        $this->addSql('CREATE SEQUENCE direccion_id_seq');
        $this->addSql('SELECT setval(\'direccion_id_seq\', (SELECT MAX(id) FROM direccion))');
        $this->addSql('ALTER TABLE direccion ALTER id SET DEFAULT nextval(\'direccion_id_seq\')');
        $this->addSql('CREATE SEQUENCE documento_id_seq');
        $this->addSql('SELECT setval(\'documento_id_seq\', (SELECT MAX(id) FROM documento))');
        $this->addSql('ALTER TABLE documento ALTER id SET DEFAULT nextval(\'documento_id_seq\')');
        $this->addSql('CREATE SEQUENCE documentos_persona_id_seq');
        $this->addSql('SELECT setval(\'documentos_persona_id_seq\', (SELECT MAX(id) FROM documentos_persona))');
        $this->addSql('ALTER TABLE documentos_persona ALTER id SET DEFAULT nextval(\'documentos_persona_id_seq\')');
        $this->addSql('ALTER TABLE cliente DROP telefono');
        $this->addSql('ALTER TABLE cliente DROP celular');
        $this->addSql('CREATE SEQUENCE cliente_id_seq');
        $this->addSql('SELECT setval(\'cliente_id_seq\', (SELECT MAX(id) FROM cliente))');
        $this->addSql('ALTER TABLE cliente ALTER id SET DEFAULT nextval(\'cliente_id_seq\')');
    }
}
