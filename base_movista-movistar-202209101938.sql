--
-- PostgreSQL database dump
--

-- Dumped from database version 13.7
-- Dumped by pg_dump version 13.7

-- Started on 2022-09-10 19:38:28

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3119 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 221 (class 1255 OID 16476)
-- Name: notify_messenger_messages(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.notify_messenger_messages() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
            BEGIN
                PERFORM pg_notify('messenger_messages', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$;


ALTER FUNCTION public.notify_messenger_messages() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 207 (class 1259 OID 16415)
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente (
    id integer NOT NULL,
    id_sexo_id integer,
    edad integer,
    primer_nombre character varying(35) NOT NULL,
    segundo_nombre character varying(35) DEFAULT NULL::character varying,
    primer_apellido character varying(35) NOT NULL,
    segundo_apellido character varying(35) DEFAULT NULL::character varying,
    fecha_nacimiento timestamp(0) without time zone NOT NULL,
    fecha_creacion timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    fecha_actualizacion timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    telefono character varying(12) DEFAULT NULL::character varying,
    celular character varying(12) DEFAULT NULL::character varying,
    otros_documentos json,
    activo boolean
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16401)
-- Name: cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cliente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_id_seq OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16424)
-- Name: correo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.correo (
    id integer NOT NULL,
    id_cliente_id integer NOT NULL,
    email character varying(50) NOT NULL
);


ALTER TABLE public.correo OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16403)
-- Name: correo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.correo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.correo_id_seq OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16569)
-- Name: departamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.departamento (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL
);


ALTER TABLE public.departamento OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16565)
-- Name: departamento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.departamento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departamento_id_seq OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16435)
-- Name: direccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.direccion (
    id integer NOT NULL,
    cliente_id integer NOT NULL,
    descripcion character varying(600) NOT NULL,
    municipio_id integer
);


ALTER TABLE public.direccion OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16407)
-- Name: direccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.direccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.direccion_id_seq OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16395)
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16444)
-- Name: documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documento (
    id integer NOT NULL,
    nombre character varying(35) NOT NULL
);


ALTER TABLE public.documento OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16409)
-- Name: documento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documento_id_seq OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16449)
-- Name: documentos_persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documentos_persona (
    id integer NOT NULL,
    id_documento_id integer NOT NULL,
    numero character varying(25) NOT NULL,
    cliente_id integer NOT NULL
);


ALTER TABLE public.documentos_persona OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16411)
-- Name: documentos_persona_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documentos_persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documentos_persona_id_seq OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16463)
-- Name: messenger_messages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messenger_messages (
    id bigint NOT NULL,
    body text NOT NULL,
    headers text NOT NULL,
    queue_name character varying(190) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    available_at timestamp(0) without time zone NOT NULL,
    delivered_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.messenger_messages OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16461)
-- Name: messenger_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.messenger_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messenger_messages_id_seq OWNER TO postgres;

--
-- TOC entry 3120 (class 0 OID 0)
-- Dependencies: 213
-- Name: messenger_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.messenger_messages_id_seq OWNED BY public.messenger_messages.id;


--
-- TOC entry 218 (class 1259 OID 16574)
-- Name: municipio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.municipio (
    id integer NOT NULL,
    departamento_id integer NOT NULL,
    nombre character varying(50) NOT NULL
);


ALTER TABLE public.municipio OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16567)
-- Name: municipio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.municipio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipio_id_seq OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16456)
-- Name: sexo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sexo (
    id integer NOT NULL,
    nombre character varying(15) NOT NULL
);


ALTER TABLE public.sexo OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16413)
-- Name: sexo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sexo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sexo_id_seq OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16603)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying(180) NOT NULL,
    roles json NOT NULL,
    password character varying(255) NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16595)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- TOC entry 2920 (class 2604 OID 16466)
-- Name: messenger_messages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger_messages ALTER COLUMN id SET DEFAULT nextval('public.messenger_messages_id_seq'::regclass);


--
-- TOC entry 3100 (class 0 OID 16415)
-- Dependencies: 207
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente (id, id_sexo_id, edad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, fecha_nacimiento, fecha_creacion, fecha_actualizacion, telefono, celular, otros_documentos, activo) FROM stdin;
115	2	0	JUNACHO	CAMANANCE	perez	aguilar	2022-08-30 00:00:00	2022-09-10 20:54:19	2022-09-11 01:12:35	131312323123	12313123	[{"NUM":"qweqew","TIPO":"qweqwe"}]	t
3	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
27	1	\N	www		wqw		2022-08-30 00:00:00	\N	\N	\N	\N	\N	t
116	1	22	Henrrique 	Eduardo	munto	uceda	2000-01-03 00:00:00	2022-09-11 03:18:17	2022-09-11 03:24:47	76534343434	77434534535	[{"NUM":"2342424","TIPO":"232323"},{"NUM":"3422342","TIPO":"2323"}]	t
4	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
5	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
6	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
7	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
8	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
9	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
10	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
11	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
12	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
13	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
14	1	2	sdads	wrwe	asdasd	werwer	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
32	1	\N	qweqwe	qweqwe	qweqwe	qweqweqwe	2022-09-08 00:00:00	\N	\N	\N	\N	\N	t
33	1	\N	qeqweqwe	qweqwe	qweqew	qweqew	2022-09-08 00:00:00	\N	\N	qweqew	qweqew	\N	t
34	1	3	qeqweqwe	qweqwe	qweqew	qweqew	2022-09-08 00:00:00	\N	\N	qweqew	qweqew	\N	t
35	1	3	qeqweqwe	qweqwe	qweqew	qweqew	2022-09-08 00:00:00	\N	\N	qweqew	qweqew	\N	t
36	1	0	qeqweqwe	qweqwe	qweqew	qweqew	2022-09-08 00:00:00	\N	\N	qweqew	qweqew	\N	t
37	1	10	qeqweqwe	qweqwe	qweqew	qweqew	2012-01-01 00:00:00	\N	\N	qweqew	qweqew	\N	t
44	1	0	primerNombre	segundoNombre	primerApellido	\N	2022-09-08 00:00:00	2022-09-09 14:23:41	\N	\N	\N	\N	t
45	1	0	qweqwe	qweqweqwe	qweqwe	\N	2022-09-06 00:00:00	2022-09-09 14:27:52	\N	4234234	234243234234	\N	t
46	1	0	primerNombre	segundoNombre	primerApellido	\N	2022-09-08 00:00:00	2022-09-09 14:29:01	\N	\N	\N	\N	t
47	1	0	qeww	qweqwe	qweqwe	\N	2022-08-31 00:00:00	2022-09-09 14:30:36	\N	312312	12312313	\N	t
48	1	0	primerNombre	segundoNombre	primerApellido	\N	2022-09-08 00:00:00	2022-09-09 14:32:11	\N	\N	\N	\N	t
49	1	0	primerNombre	segundoNombre	primerApellido	\N	2022-09-08 00:00:00	2022-09-09 14:32:39	\N	\N	\N	\N	t
50	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 14:34:53	\N	\N	\N	\N	t
51	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 14:40:44	\N	\N	\N	\N	t
52	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 14:41:01	\N	\N	\N	\N	t
53	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 14:41:41	\N	\N	\N	\N	t
54	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 14:42:12	\N	\N	\N	\N	t
55	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 15:00:23	\N	\N	\N	\N	t
56	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 15:01:07	\N	\N	\N	\N	t
57	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 15:01:27	\N	\N	\N	\N	t
58	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 15:06:04	\N	\N	\N	\N	t
59	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 16:02:10	\N	\N	\N	\N	t
60	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 16:02:53	\N	\N	\N	\N	t
61	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 16:03:35	\N	\N	\N	\N	t
62	1	0	12313	23132123	123213123	123123132	2022-09-09 00:00:00	2022-09-09 16:13:07	\N	73659816	212121212	\N	t
63	1	0	12313	23132123	123213123	123123132	2022-09-09 00:00:00	2022-09-09 16:13:20	\N	73659816	212121212	\N	t
64	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 16:13:40	\N	\N	\N	\N	t
65	1	0	qweqeqew	qqeqwe	qeqew	qweqwe	2022-09-09 00:00:00	2022-09-09 16:19:39	\N	73659816	123121	\N	t
66	1	0	qweqeqew	qqeqwe	qeqew	qweqwe	2022-09-09 00:00:00	2022-09-09 16:19:56	\N	73659816	123121	\N	t
67	1	0	ewqewqe	qweqweqew	qewqew	qweqweqwe	2022-09-09 00:00:00	2022-09-09 16:21:15	\N	123123	123123123	\N	t
92	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-10 00:58:59	\N	\N	\N	\N	t
94	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-10 00:59:48	\N	\N	\N	\N	t
95	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-10 01:59:13	\N	\N	\N	\N	t
68	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 16:25:36	\N	\N	\N	\N	t
69	1	0	qweqewq	Alex Uceda Munto	qweqweqw	Uceda	2022-09-09 00:00:00	2022-09-09 17:04:25	\N	12313132	132123	\N	t
70	1	0	qweqweq	Alex Uceda Munto	qewqwe	Uceda	2022-09-09 00:00:00	2022-09-09 17:06:23	\N	73659816	32323	\N	t
71	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 17:16:09	\N	\N	\N	\N	t
72	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 17:20:53	\N	\N	\N	\N	t
73	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 17:37:17	\N	\N	\N	\N	t
74	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 17:37:47	\N	\N	\N	\N	t
75	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 17:41:21	\N	\N	\N	\N	t
77	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 17:57:33	\N	\N	\N	\N	t
78	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-09 17:58:15	\N	\N	\N	\N	t
79	1	0	ewqewqe	qweqweqew	qewqew	qweqweqwe	2022-09-09 00:00:00	2022-09-09 17:58:22	\N	123123	123123123	\N	t
80	1	0	wqeqweqwe	qewqew	qweqweq	qewqweqwe	2022-09-09 00:00:00	2022-09-09 18:01:52	\N	qeqweqw	qweqeqewqw	\N	t
81	1	0	qweqeqw	qweqwqwe	qwqweeqw	qweqwe	2022-09-09 00:00:00	2022-09-09 18:11:00	\N	12123123	123123	\N	t
83	1	0	23223232323	Alex Uceda Munto	23233223	Uceda	2022-08-29 00:00:00	2022-09-09 19:30:08	\N	73659816	223232323	\N	t
85	1	0	23223232323	Alex Uceda Munto	23233223	Uceda	2022-08-29 00:00:00	2022-09-09 19:30:48	\N	73659816	223232323	\N	t
20	1	\N	qweqwe	qweqwe	qweqwe	qweqweqwe	2022-09-08 00:00:00	\N	\N	\N	\N	\N	t
21	1	\N	asdasd	asdasd	asda	dasasd	2022-09-06 00:00:00	\N	\N	\N	\N	\N	t
22	1	\N	qweqwe	qwe	qwe	qwe	2022-08-29 00:00:00	\N	\N	\N	\N	\N	t
23	1	\N	asdasd	asdasd	asdasd	ad43434	2022-08-30 00:00:00	\N	\N	\N	\N	\N	t
24	1	\N	sdasd	asdasd	asd	sas	2022-09-08 00:00:00	\N	\N	\N	\N	\N	t
25	1	\N	sdasd	asdasd	asdssss	sas	2022-09-08 00:00:00	\N	\N	\N	\N	\N	t
26	1	\N	asdasdqweqweqweqweqewqweqweqweqweqw	asdasdqweqweqweqweqewqweqweqweqweqw	asdasdqweqweqweqweqewqweqweqweqweqw	sasqweqweqweqweqweqweqweqweqweqeqwe	2022-08-30 00:00:00	\N	\N	\N	\N	\N	t
86	1	0	xxxxxx	xxxxxxx	xxxxxx	xxxxxx	2022-08-28 00:00:00	2022-09-09 21:12:40	\N	xxxxxxxx	xxxxx	\N	t
2	1	2	xxxxx	xxxx	xxxx	xxxxxxx	1993-02-02 00:00:00	\N	\N	\N	\N	\N	t
87	1	0	zzzz	zzzzzzzzz	zzzzzzz	zzzzzzzz	2022-08-29 00:00:00	2022-09-09 22:01:07	\N	33233424	33242343	\N	t
88	1	0	alex	alfonso	munto	uceda	2022-09-09 00:00:00	2022-09-09 23:00:07	\N	73659816	73659816	\N	t
89	1	0	aaaaaa	bbbbbbb	ccccccc	dddddd	2022-08-29 00:00:00	2022-09-10 00:14:35	\N	73659816	73659816	\N	t
90	1	0	aaaaaa	bbbbbbb	ccccccc	dddddd	2022-08-29 00:00:00	2022-09-10 00:15:18	\N	73659816	73659816	\N	t
84	1	0	23223232323	Alex Uceda Munto	23233223	Uceda	2022-08-29 00:00:00	2022-09-09 19:30:37	\N	73659816	223232323	\N	t
97	1	0	qweqewqew	qweqew	qeqewqew	qweqewqew	2022-08-30 00:00:00	2022-09-10 02:25:44	\N	ewqweqwe	qwqweqewqwee	\N	t
98	1	0	qweqewqew	qweqew	qeqewqew	qweqewqew	2022-08-30 00:00:00	2022-09-10 02:36:55	\N	ewqweqwe	qwqweqewqwee	\N	t
99	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-10 04:20:20	\N	\N	\N	\N	t
100	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-10 04:30:09	\N	\N	\N	\N	t
101	1	0	primerNombre	segundoNombre	primerApellido	segundoApellido	2022-09-08 00:00:00	2022-09-10 04:36:41	\N	\N	\N	\N	t
91	2	0	ffffff	fffff	ffff	segundoApellido	2022-09-08 00:00:00	2022-09-10 00:17:33	2022-09-10 04:36:47	73659816	73659816	\N	t
102	1	0	rrrrrrrrrrrrr	rrrrrrrrrrrrrr	tttttttttttttttt	tttttttttttt	2022-08-30 00:00:00	2022-09-10 04:46:00	\N	4444444444	55555555555	\N	t
103	1	0	rrrrrrrrrrrrr	rrrrrrrrrrrrrr	tttttttttttttttt	tttttttttttt	2022-08-30 00:00:00	2022-09-10 04:55:55	\N	4444444444	55555555555	\N	t
104	1	0	rrrrrrrrrrrrr	rrrrrrrrrrrrrr	tttttttttttttttt	tttttttttttt	2022-08-30 00:00:00	2022-09-10 04:56:12	\N	4444444444	55555555555	\N	t
105	1	0	rrrrrrrrrrrrr	rrrrrrrrrrrrrr	tttttttttttttttt	tttttttttttt	2022-08-30 00:00:00	2022-09-10 04:57:21	\N	4444444444	55555555555	\N	t
106	1	0	sssssssssssssssssss	ssssssssssssssssssssss	ssssssssssssssssss	ssssssssssssss	2022-08-31 00:00:00	2022-09-10 05:01:32	\N	4444444444	55555555555	\N	t
107	1	0	333333333333	4444444	44444444444	55555555555	2022-08-30 00:00:00	2022-09-10 05:07:56	\N	44444444444	55555555555	\N	t
108	1	0	xxxxxxxxxx	xxxxxxxxx	xxxxxxx	xxxxxxxxx	2022-08-02 00:00:00	2022-09-10 05:08:36	\N	4444444444	55555555555	\N	t
109	1	0	ytrrtrtyrtyryrtryrtytryr	rrrrrrrrrrrrr	rrrrrrrrrrrrr	rrrrrrrrrr	2022-08-02 00:00:00	2022-09-10 05:09:58	2022-09-10 06:10:10	4444444444	55555555555	\N	t
110	1	22	alex	alfonso	munto	uceda	2000-02-02 00:00:00	2022-09-10 06:15:35	\N	73659816	73659816	\N	t
111	1	28	eduardo	herrique	munto	uceda	1994-09-09 00:00:00	2022-09-10 06:21:18	\N	72623065	445553233233	\N	t
114	1	0	mario22	antonio22	cabrio	Uceda	2022-08-29 00:00:00	2022-09-10 19:56:35	2022-09-11 01:10:31	73659816	567890	[{"NUM":"333","TIPO":"33333"}]	t
117	1	0	qweqeqweqe	qewqwe	qewqewqeqwe	qewqewqew	2022-08-29 00:00:00	2022-09-11 03:25:19	\N	23121231	123123123	[{"NUM":"123123","TIPO":"123123"}]	t
113	1	0	juan	marquez	de la villa	umanzor	2022-09-07 00:00:00	2022-09-10 19:54:32	2022-09-10 21:10:09	736875498	736875498	[{"NUM":"444444","TIPO":"66666"},{"NUM":"7777","TIPO":"8888"}]	f
112	2	0	juan	antonio	martinez	qweqweqwe	2022-09-07 00:00:00	2022-09-10 06:27:33	2022-09-10 21:10:17	73659816	73659816	[{"NUM":"ASDSDASDADS","TIPO":"SDASDASASD"}]	t
\.


--
-- TOC entry 3101 (class 0 OID 16424)
-- Dependencies: 208
-- Data for Name: correo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.correo (id, id_cliente_id, email) FROM stdin;
2	59	211212@wqwqweqwe
3	60	211212@wqwqweqwe
4	61	211212@wqwqweqwe
5	64	211212@wqwqweqwe
6	67	asdsd@asa
7	67	qewqw@ewqw
8	68	211212@wqwqweqwe
9	69	mu13002@ues.edu.sv
10	69	mu13002@ues.edu.sv
11	70	alexafonsomunto@gmail.com
12	70	alexafonsomunto@gmail.com
13	71	211212@wqwqweqwe
14	72	211212@wqwqweqwe
15	73	211212@wqwqweqwe
16	74	211212@wqwqweqwe
17	75	211212@wqwqweqwe
19	77	211212@wqwqweqwe
20	78	211212@wqwqweqwe
21	79	asdsd@asa
22	79	qewqw@ewqw
23	80	asdasasdasd@sadasdasdas
24	80	asdasd@sadasdads
25	81	
26	81	qweqweqe@sqwqeqw
29	83	alexafonsomunto@gmail.com
30	83	mu13002@ues.edu.sv
31	84	alexafonsomunto@gmail.com
32	84	mu13002@ues.edu.sv
35	3	xxxxnto@gmail.com
34	3	mu13002@ues.edu.sv
33	3	alexafonsomunto@gmail.com
36	87	
37	87	mu13002@ues.edu.sv
38	88	alexafonsomunto@gmail.com
39	88	mu13002@ues.edu.sv
40	89	
41	89	alexafonsomunto@gmail.com
42	90	
43	90	alexafonsomunto@gmail.com
44	90	alexafonsomunto@gmail.com
47	92	211212@wqwqweqwe
48	94	211212@wqwqweqwe
49	95	211212@wqwqweqwe
45	91	xx@ffff
46	91	xx@fff
52	97	
53	98	mu13002@ues.edu.sv
54	98	alexafonsomunto@gmail.com
55	99	211212@wqwqweqwe
56	100	211212@wqwqweqwe
57	101	211212@wqwqweqwe
80	109	qewqweqeqw@sasasddasa
81	109	asdasdadad@qwweeeee
82	109	qweqweqew@1
83	109	qweqewqwe@2
84	110	alexafonsomunto@gmail.com
85	111	6666666@erwewe
86	111	sadas@wqeqw
93	112	alexafonsomunto@gmail.com
94	112	44444@wqwqeqwe
97	113	alexafonsomunto@gmail.com
98	113	alexuceda@hotmail.com
99	114	alexafonsomunto@gmail.com
104	115	qeqwqewqwe@wqeqw
107	116	11111@qeqeeqw
108	116	2222@adadad
109	117	12312312@weqqewwe
\.


--
-- TOC entry 3110 (class 0 OID 16569)
-- Dependencies: 217
-- Data for Name: departamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.departamento (id, nombre) FROM stdin;
1	San Salvador
2	Sonsonate
3	Santa Ana
\.


--
-- TOC entry 3102 (class 0 OID 16435)
-- Dependencies: 209
-- Data for Name: direccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.direccion (id, cliente_id, descripcion, municipio_id) FROM stdin;
66	113	urb. San Carlos, apartamento 34 ed. D San Salvador	1
67	113	urb. San Carlos, apartamento 34 ed. D San Salvador	4
68	114	urb. San Carlos, apartamento 34 ed. D San Salvador	2
73	115	XXXXXXXASDASDADADS	2
75	116	342423423424	1
76	117	12312\r\n                                                3	1
63	112	4534534534	2
5	84	descripconasdadsadsdas	\N
6	85	descripconasdadsadsdas	4
7	85	descripconasdadsadsdas	\N
8	86	descripconasdadsadsdas	1
2	3	descripconasdadsadsdas	\N
3	3	descripconasdadsadsdas	1
4	3	descripconasdadsadsdas	2
9	87	descripconasdadsadsdas	1
10	87	descripconasdadsadsdas	\N
11	88	descripconasdadsadsdas	2
12	88	descripconasdadsadsdas	\N
13	89	descripconasdadsadsdas	3
14	89	descripconasdadsadsdas	\N
15	90	descripconasdadsadsdas	3
16	90	descripconasdadsadsdas	\N
21	97	descripconasdadsadsdas	1
22	98	descripconasdadsadsdas	\N
23	98	descripconasdadsadsdas	\N
17	91	descripconasdadsadsdas	1
48	109	descripconasdadsadsdas	1
49	109	descripconasdadsadsdas	2
50	109	descripconasdadsadsdas	4
51	109	descripconasdadsadsdas	1
52	110	descripconasdadsadsdas	1
53	110	descripconasdadsadsdas	1
54	111	descripconasdadsadsdas	2
55	111	descripconasdadsadsdas	1
62	112	sdfsdfsdf	1
\.


--
-- TOC entry 3093 (class 0 OID 16395)
-- Dependencies: 200
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
DoctrineMigrations\\Version20220908093443	2022-09-08 11:36:32	314
DoctrineMigrations\\Version20220908095557	2022-09-08 11:57:14	48
DoctrineMigrations\\Version20220908095934	2022-09-08 11:59:43	26
DoctrineMigrations\\Version20220908100317	2022-09-08 12:03:25	33
DoctrineMigrations\\Version20220908100715	2022-09-08 12:07:21	26
DoctrineMigrations\\Version20220908101057	2022-09-08 12:11:04	32
DoctrineMigrations\\Version20220908101541	2022-09-08 12:15:53	23
DoctrineMigrations\\Version20220908101718	2022-09-08 12:17:24	22
DoctrineMigrations\\Version20220908115545	2022-09-08 13:55:56	47
DoctrineMigrations\\Version20220908195536	2022-09-08 21:56:30	197
DoctrineMigrations\\Version20220908220322	2022-09-09 00:03:29	48
DoctrineMigrations\\Version20220909155235	2022-09-09 17:54:10	39
DoctrineMigrations\\Version20220909155404	2022-09-09 18:35:01	21
DoctrineMigrations\\Version20220909162736	2022-09-09 18:35:01	37
DoctrineMigrations\\Version20220909163602	2022-09-09 18:36:12	20
DoctrineMigrations\\Version20220909231531	2022-09-10 01:15:40	206
DoctrineMigrations\\Version20220910050201	2022-09-10 07:02:28	207
DoctrineMigrations\\Version20220910181152	2022-09-10 20:12:04	197
DoctrineMigrations\\Version20220910233331	2022-09-11 01:34:08	53
DoctrineMigrations\\Version20220910235512	2022-09-11 01:55:32	55
\.


--
-- TOC entry 3103 (class 0 OID 16444)
-- Dependencies: 210
-- Data for Name: documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documento (id, nombre) FROM stdin;
1	dui
2	nit
3	isss
\.


--
-- TOC entry 3104 (class 0 OID 16449)
-- Dependencies: 211
-- Data for Name: documentos_persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documentos_persona (id, id_documento_id, numero, cliente_id) FROM stdin;
4	1	123131231	80
5	2	2121	80
6	3	123123	80
7	1	1212312	81
8	2	3434344	81
10	1	3232	83
11	1	3232	84
12	1	3232	85
13	1	3234234234	86
1	3	1231	77
2	3	1231	78
3	3	1231	78
14	2	33	87
15	2	33	87
16	1	04895886-5	88
17	1	124487952	88
18	1	11111111	89
19	2	22222222222	89
20	1	11111111	90
21	2	22222222222	90
24	1	1231	92
25	2	1231	92
26	1	1231	94
27	2	1231	94
28	1	1231	95
29	2	1231	95
22	1	fffff	91
23	2	ffffff	91
30	1	123123132312	97
31	1	33	98
32	2	2332323	98
33	1	1231	99
34	2	1231	99
35	1	1231	100
36	2	1231	100
37	1	1231	101
38	2	1231	101
50	2	44444	109
51	2	555	109
52	1	33	110
53	1	666666666	111
57	1	1312131231	112
60	1	259644	113
61	2	12345678	113
62	1	04895886-7	114
67	1	33	115
69	1	234234	116
70	1	1231212	117
\.


--
-- TOC entry 3107 (class 0 OID 16463)
-- Dependencies: 214
-- Data for Name: messenger_messages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.messenger_messages (id, body, headers, queue_name, created_at, available_at, delivered_at) FROM stdin;
\.


--
-- TOC entry 3111 (class 0 OID 16574)
-- Dependencies: 218
-- Data for Name: municipio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.municipio (id, departamento_id, nombre) FROM stdin;
1	1	San Salvador
2	1	Mejicanos
3	3	Santa Rosa
4	2	Izalco
5	2	Armenia
\.


--
-- TOC entry 3105 (class 0 OID 16456)
-- Dependencies: 212
-- Data for Name: sexo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sexo (id, nombre) FROM stdin;
1	masculino
2	femenino
\.


--
-- TOC entry 3113 (class 0 OID 16603)
-- Dependencies: 220
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, email, roles, password) FROM stdin;
1	alex@gmail.com	[]	$2y$13$/WQaAKxzJ9.P/3AmmoL4zePF.U86s/.vGqOvz5fQQUfX6hl8D7b66
\.


--
-- TOC entry 3121 (class 0 OID 0)
-- Dependencies: 201
-- Name: cliente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cliente_id_seq', 117, true);


--
-- TOC entry 3122 (class 0 OID 0)
-- Dependencies: 202
-- Name: correo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.correo_id_seq', 109, true);


--
-- TOC entry 3123 (class 0 OID 0)
-- Dependencies: 215
-- Name: departamento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.departamento_id_seq', 1, false);


--
-- TOC entry 3124 (class 0 OID 0)
-- Dependencies: 203
-- Name: direccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.direccion_id_seq', 76, true);


--
-- TOC entry 3125 (class 0 OID 0)
-- Dependencies: 204
-- Name: documento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.documento_id_seq', 3, true);


--
-- TOC entry 3126 (class 0 OID 0)
-- Dependencies: 205
-- Name: documentos_persona_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.documentos_persona_id_seq', 70, true);


--
-- TOC entry 3127 (class 0 OID 0)
-- Dependencies: 213
-- Name: messenger_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.messenger_messages_id_seq', 1, false);


--
-- TOC entry 3128 (class 0 OID 0)
-- Dependencies: 216
-- Name: municipio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.municipio_id_seq', 1, false);


--
-- TOC entry 3129 (class 0 OID 0)
-- Dependencies: 206
-- Name: sexo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sexo_id_seq', 4, true);


--
-- TOC entry 3130 (class 0 OID 0)
-- Dependencies: 219
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 1, false);


--
-- TOC entry 2925 (class 2606 OID 16421)
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id);


--
-- TOC entry 2928 (class 2606 OID 16428)
-- Name: correo correo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.correo
    ADD CONSTRAINT correo_pkey PRIMARY KEY (id);


--
-- TOC entry 2948 (class 2606 OID 16573)
-- Name: departamento departamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamento
    ADD CONSTRAINT departamento_pkey PRIMARY KEY (id);


--
-- TOC entry 2931 (class 2606 OID 16442)
-- Name: direccion direccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT direccion_pkey PRIMARY KEY (id);


--
-- TOC entry 2923 (class 2606 OID 16400)
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- TOC entry 2935 (class 2606 OID 16448)
-- Name: documento documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento
    ADD CONSTRAINT documento_pkey PRIMARY KEY (id);


--
-- TOC entry 2937 (class 2606 OID 16453)
-- Name: documentos_persona documentos_persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documentos_persona
    ADD CONSTRAINT documentos_persona_pkey PRIMARY KEY (id);


--
-- TOC entry 2946 (class 2606 OID 16472)
-- Name: messenger_messages messenger_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger_messages
    ADD CONSTRAINT messenger_messages_pkey PRIMARY KEY (id);


--
-- TOC entry 2951 (class 2606 OID 16578)
-- Name: municipio municipio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.municipio
    ADD CONSTRAINT municipio_pkey PRIMARY KEY (id);


--
-- TOC entry 2941 (class 2606 OID 16460)
-- Name: sexo sexo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sexo
    ADD CONSTRAINT sexo_pkey PRIMARY KEY (id);


--
-- TOC entry 2954 (class 2606 OID 16610)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2938 (class 1259 OID 16454)
-- Name: idx_1563c9a86601ba07; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_1563c9a86601ba07 ON public.documentos_persona USING btree (id_documento_id);


--
-- TOC entry 2939 (class 1259 OID 16564)
-- Name: idx_1563c9a8de734e51; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_1563c9a8de734e51 ON public.documentos_persona USING btree (cliente_id);


--
-- TOC entry 2942 (class 1259 OID 16475)
-- Name: idx_75ea56e016ba31db; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_75ea56e016ba31db ON public.messenger_messages USING btree (delivered_at);


--
-- TOC entry 2943 (class 1259 OID 16474)
-- Name: idx_75ea56e0e3bd61ce; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_75ea56e0e3bd61ce ON public.messenger_messages USING btree (available_at);


--
-- TOC entry 2944 (class 1259 OID 16473)
-- Name: idx_75ea56e0fb7336f0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_75ea56e0fb7336f0 ON public.messenger_messages USING btree (queue_name);


--
-- TOC entry 2929 (class 1259 OID 16429)
-- Name: idx_77040bc97bf9ce86; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_77040bc97bf9ce86 ON public.correo USING btree (id_cliente_id);


--
-- TOC entry 2932 (class 1259 OID 16590)
-- Name: idx_f384be9558bc1be0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f384be9558bc1be0 ON public.direccion USING btree (municipio_id);


--
-- TOC entry 2933 (class 1259 OID 16443)
-- Name: idx_f384be95de734e51; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f384be95de734e51 ON public.direccion USING btree (cliente_id);


--
-- TOC entry 2926 (class 1259 OID 16423)
-- Name: idx_f41c9b25f5af7228; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f41c9b25f5af7228 ON public.cliente USING btree (id_sexo_id);


--
-- TOC entry 2949 (class 1259 OID 16579)
-- Name: idx_fe98f5e05a91c08d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fe98f5e05a91c08d ON public.municipio USING btree (departamento_id);


--
-- TOC entry 2952 (class 1259 OID 16611)
-- Name: uniq_8d93d649e7927c74; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_8d93d649e7927c74 ON public."user" USING btree (email);


--
-- TOC entry 2962 (class 2620 OID 16477)
-- Name: messenger_messages notify_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON public.messenger_messages FOR EACH ROW EXECUTE FUNCTION public.notify_messenger_messages();


--
-- TOC entry 2959 (class 2606 OID 16498)
-- Name: documentos_persona fk_1563c9a86601ba07; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documentos_persona
    ADD CONSTRAINT fk_1563c9a86601ba07 FOREIGN KEY (id_documento_id) REFERENCES public.documento(id);


--
-- TOC entry 2960 (class 2606 OID 16559)
-- Name: documentos_persona fk_1563c9a8de734e51; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documentos_persona
    ADD CONSTRAINT fk_1563c9a8de734e51 FOREIGN KEY (cliente_id) REFERENCES public.cliente(id);


--
-- TOC entry 2956 (class 2606 OID 16488)
-- Name: correo fk_77040bc97bf9ce86; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.correo
    ADD CONSTRAINT fk_77040bc97bf9ce86 FOREIGN KEY (id_cliente_id) REFERENCES public.cliente(id);


--
-- TOC entry 2958 (class 2606 OID 16585)
-- Name: direccion fk_f384be9558bc1be0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT fk_f384be9558bc1be0 FOREIGN KEY (municipio_id) REFERENCES public.municipio(id);


--
-- TOC entry 2957 (class 2606 OID 16493)
-- Name: direccion fk_f384be95de734e51; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT fk_f384be95de734e51 FOREIGN KEY (cliente_id) REFERENCES public.cliente(id);


--
-- TOC entry 2955 (class 2606 OID 16483)
-- Name: cliente fk_f41c9b25f5af7228; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT fk_f41c9b25f5af7228 FOREIGN KEY (id_sexo_id) REFERENCES public.sexo(id);


--
-- TOC entry 2961 (class 2606 OID 16580)
-- Name: municipio fk_fe98f5e05a91c08d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.municipio
    ADD CONSTRAINT fk_fe98f5e05a91c08d FOREIGN KEY (departamento_id) REFERENCES public.departamento(id);


-- Completed on 2022-09-10 19:38:29

--
-- PostgreSQL database dump complete
--

