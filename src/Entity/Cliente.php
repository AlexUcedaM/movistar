<?php

namespace App\Entity;

use App\Repository\ClienteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Com;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTime;


/**
 * @ORM\Entity(repositoryClass=ClienteRepository::class)
 * @ApiResource(
 *      denormalizationContext={
 *          "groups"={"post"},
 *      },
 *      collectionOperations={
 *          "get",
 *          "post",
 *      },
 *      attributes={"order"={"id": "DESC"}},
 *      description="Provee metodos para la gestion de clientes.",
 *      formats={"json"},
 *      
 *  )
 * @ORM\HasLifecycleCallbacks()
 */
class Cliente
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({  "post"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({ "post" })
     */
    private $edad;

    /**
     * @ORM\Column(type="string", length=35)
     * @Groups({ "post" })
     */
    private $primerNombre;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     * @Groups({ "post" })
     * @ApiFilter(SearchFilter::class, strategy="partial")
     */
    private $segundoNombre;

    /**
     * @ORM\Column(type="string", length=35)
     * @Groups({ "post" })
     * @ApiFilter(SearchFilter::class, strategy="partial")
     */
    private $primerApellido;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     * @Groups({ "post" })
     * @ApiFilter(SearchFilter::class, strategy="partial")
     */
    private $segundoApellido;


    /**
     * @ORM\OneToMany(targetEntity=Correo::class, mappedBy="idCliente", orphanRemoval=true, cascade={"persist"})
     * @Groups({ "post", "get" })
     */
    private $correos;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({ "post" })
     */
    private $fechaNacimiento;

    /**
     * @ORM\ManyToOne(targetEntity=Sexo::class)
     * @Groups({ "post" })
     */
    private $idSexo;

    /**
     * @ORM\OneToMany(targetEntity=Direccion::class, mappedBy="cliente", orphanRemoval=true, cascade={"persist"}))
     * @Groups({ "post", "get" })
     */
    private $idDireccion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({ "post" })
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({ "post" })
     */
    private $fechaActualizacion;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     * @Groups({ "post" })
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     * @Groups({ "post" })
     */
    private $celular;

    /**
     * @ORM\OneToMany(targetEntity=DocumentosPersona::class, mappedBy="cliente", orphanRemoval=true, cascade={"persist"})
     * @Groups({ "post", "get" })
     */
    private $documentos;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @Groups({ "post", "get" })
     */
    private $otrosDocumentos = [];

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({ "post", "get" })
     */
    private $activo;

    public function __construct()
    {
        
        $this->correos = new ArrayCollection();
        $this->idDireccion = new ArrayCollection();
        $this->documentos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEdad(): ?int
    {
        return $this->edad;
    }

    public function setEdad(?int $edad): self
    {
        $this->edad = $edad;

        return $this;
    }

    public function getPrimerNombre(): ?string
    {
        return $this->primerNombre;
    }

    public function setPrimerNombre(string $primerNombre): self
    {
        $this->primerNombre = $primerNombre;

        return $this;
    }

    public function getSegundoNombre(): ?string
    {
        return $this->segundoNombre;
    }

    public function setSegundoNombre(?string $segundoNombre): self
    {
        $this->segundoNombre = $segundoNombre;

        return $this;
    }

    public function getPrimerApellido(): ?string
    {
        return $this->primerApellido;
    }

    public function setPrimerApellido(string $primerApellido): self
    {
        $this->primerApellido = $primerApellido;

        return $this;
    }

    public function getSegundoApellido(): ?string
    {
        return $this->segundoApellido;
    }

    public function setSegundoApellido(?string $segundoApellido): self
    {
        $this->segundoApellido = $segundoApellido;

        return $this;
    }


    /**
     * @return Collection<int, Correo>
     */
    public function getCorreos(): Collection
    {
        return $this->correos;
    }

    public function addCorreo(Correo $correo): self
    {
        if (!$this->correos->contains($correo)) {
            $this->correos[] = $correo;
            $correo->setIdCliente($this);
        }

        return $this;
    }

    public function removeCorreo(Correo $correo): self
    {
        if ($this->correos->removeElement($correo)) {
            // set the owning side to null (unless already changed)
            if ($correo->getIdCliente() === $this) {
                $correo->setIdCliente(null);
            }
        }

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento(\DateTimeInterface $fechaNacimiento): self
    {
        $this->fechaNacimiento = $fechaNacimiento;
        $now = new DateTime();
        $interval = $now->diff($fechaNacimiento);
        $this->setEdad($interval->y);
        return $this;
    }


    public function getIdSexo(): ?Sexo
    {
        return $this->idSexo;
    }

    public function setIdSexo(?Sexo $idSexo): self
    {
        $this->idSexo = $idSexo;

        return $this;
    }

    /**
     * @return Collection<int, Direccion>
     */
    public function getIdDireccion(): Collection
    {
        return $this->idDireccion;
    }

    public function addIdDireccion(Direccion $idDireccion): self
    {
        if (!$this->idDireccion->contains($idDireccion)) {
            $this->idDireccion[] = $idDireccion;
            $idDireccion->setCliente($this);
        }

        return $this;
    }

    public function removeIdDireccion(Direccion $idDireccion): self
    {
        if ($this->idDireccion->removeElement($idDireccion)) {
            // set the owning side to null (unless already changed)
            if ($idDireccion->getCliente() === $this) {
                $idDireccion->setCliente(null);
            }
        }

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    /**
     * @ORM\PrePersist
    */
    public function setFechaCreacion(): self
    {
        $this->fechaCreacion = new DateTime();

        return $this;
    }

    /**
     * @ORM\PrePersist
    */
    public function activar(): self
    {
        $this->activo = true;
        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    /**
     * @ORM\PreUpdate
    */
    public function setFechaActualizacion(): self
    {
        $this->fechaActualizacion = new DateTime();

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCelular(): ?string
    {
        return $this->celular;
    }

    public function setCelular(?string $celular): self
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * @return Collection<int, DocumentosPersona>
     */
    public function getDocumentos(): Collection
    {
        return $this->documentos;
    }

    public function addDocumento(DocumentosPersona $documento): self
    {
        if (!$this->documentos->contains($documento)) {
            $this->documentos[] = $documento;
            $documento->setCliente($this);
        }

        return $this;
    }

    public function removeDocumento(DocumentosPersona $documento): self
    {
        if ($this->documentos->removeElement($documento)) {
            // set the owning side to null (unless already changed)
            if ($documento->getCliente() === $this) {
                $documento->setCliente(null);
            }
        }

        return $this;
    }

    public function getOtrosDocumentos(): ?array
    {
        return $this->otrosDocumentos;
    }

    public function setOtrosDocumentos(?array $otrosDocumentos): self
    {
        $this->otrosDocumentos = $otrosDocumentos;

        return $this;
    }

    public function isActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(?bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }
}
