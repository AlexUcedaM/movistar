<?php

namespace App\Entity;

use App\Repository\CorreoRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CorreoRepository::class)
 * @ApiResource(
 *      collectionOperations={
 *          "get",
 *          "post"
 *      },
 *      normalizationContext=
 *          {"groups"={"norm"}},
 *      denormalizationContext=
 *          {"groups"={"post"}}
 * )
 */
class Correo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *      "post", "get", "norm" 
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({
     *      "post", "get", "norm" 
     * })
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity=Cliente::class, inversedBy="correos")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "norm"
     *  })
     */
    private $idCliente;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIdCliente(): ?Cliente
    {
        return $this->idCliente;
    }

    public function setIdCliente(?Cliente $idCliente): self
    {
        $this->idCliente = $idCliente;

        return $this;
    }
}
