<?php

namespace App\Entity;

use App\Repository\DocumentoRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DocumentoRepository::class)
 * @ApiResource(
 *      collectionOperations={
 *          "get",
 *          "post"
 *      },
 *      normalizationContext=
 *          {"groups"={"norm"}},
 *      denormalizationContext=
 *          {"groups"={"post"}}
 * )
 */
class Documento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @Groups({
     *     "get", "norm" 
     * }) 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=35)
     * @Groups({
     *     "get", "norm" 
     * })
     */
    private $nombre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    function __toString()
    {
        return $this->nombre;
    }
}
