<?php

namespace App\Entity;

use App\Repository\DireccionRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DireccionRepository::class)
 * @ApiResource(
 *      collectionOperations={
 *          "get",
 *          "post"
 *      },
 *      normalizationContext=
 *          {"groups"={"norm"}},
 *      denormalizationContext=
 *          {"groups"={"post"}}
 * )
 */
class Direccion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *      "post", "get", "norm" 
     *  })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=600)
     * @Groups({
     *      "post", "get", "norm" 
     *  })
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity=Cliente::class, inversedBy="idDireccion")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *      "get", "norm" 
     *  })
     */
    private $cliente;

    /**
     * @ORM\ManyToOne(targetEntity=Municipio::class)
     * @Groups({
     *      "post", "get", "norm" 
     *  })
     */
    private $municipio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    public function setCliente(?Cliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getMunicipio(): ?Municipio
    {
        return $this->municipio;
    }

    public function setMunicipio(?Municipio $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }
}
