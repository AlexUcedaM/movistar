<?php

namespace App\Entity;

use App\Repository\DocumentosPersonaRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DocumentosPersonaRepository::class)
 * @ApiResource(
 *      collectionOperations={
 *          "get",
 *          "post"
 *      },
 *      normalizationContext=
 *          {"groups"={"norm"}},
 *      denormalizationContext=
 *          {"groups"={"post"}},
 * 
 * )
 */
class DocumentosPersona
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *      "post", "get", "norm" 
     *  })
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Documento::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *      "post", "get", "norm" 
     *  })
     */
    private $idDocumento;


    /**
     * @ORM\Column(type="string", length=25)
     * @Groups({
     *      "post", "get", "norm" 
     *  })
     */
    private $numero;

    /**
     * @ORM\ManyToOne(targetEntity=Cliente::class, inversedBy="documentos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cliente;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdDocumento(): ?Documento
    {
        return $this->idDocumento;
    }

    public function setIdDocumento(?Documento $idDocumento): self
    {
        $this->idDocumento = $idDocumento;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    public function setCliente(?Cliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }
}
