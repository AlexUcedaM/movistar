<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ClienteRepository;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\HttpFoundation\Request;

class ApiClienteController extends AbstractController
{


    /**
     * @Route("/api_v1/cliente_ejemplo", name="app_api_cliente_ejemplo" , methods={"GET"})
     */
    public function index( ClienteRepository $clienteRepository): Response
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $data= $clienteRepository->findAll()[0];

        $jsonContent = $serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
             }
        ]);
     
        $response = new Response( 
            json_encode( 
                array( 
                    'exito' => true, 
                    'data'=>$jsonContent )
                )
            );
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/api_v1/cliente/{format}/{pagina}", name="app_api_cliente" , methods={"GET"})
     * * @Route("/api_v1/cliente/{format}", name="app_api_cliente_v2" , methods={"GET"})
     */
    public function indexv2( $format, $pagina=1, ClienteRepository $clienteRepository): Response
    {
        /* 
            FORMA A PIE DE CREAR LA API, ERA MUCHO MAS SENCILLO Y UTIL GENERARLA CON API PLATAFORM
        */
        if( ! in_array($format, Array('xml', 'json'))){
            $response = new Response(
                json_encode(
                    Array(
                        'error'=>'Necesita un formato valido',
                    )
                )
            );
            return $response->headers->set('Content-Type', 'application/json');
        }
        

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $data= $clienteRepository->findAll();

        $jsonContent = $serializer->serialize($data, $format, [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
             }
        ]);

        $jsonContent = '{"status": true,"data": '.$jsonContent.'}';
     
        $response = new Response( 
            $jsonContent 
        );
        $response->headers->set('Content-Type', 'application/'.$format);
        return $response;
        
    }


    /**
     * @Route("/api_v3/cliente/{pagina}", name="app_api_clienteV3" , methods={"GET"})
     * * @Route("/api_v3/cliente/", name="app_api_cliente_V3" , methods={"GET"})
     */
    public function index_v3(Request $request, $pagina=1, $nombre_o_dui='', ClienteRepository $clienteRepository): Response
    {
        $nombre_o_dui = $request->query->get('nombre_o_dui');
        
        $data= $clienteRepository->getListaClientes($pagina, $nombre_o_dui);

        return new JsonResponse(  $data );
    }

    /**
     * @Route("/api_v3/nombres", name="app_api_cliente_nombre" , methods={"GET"})
     */
    public function index_v3_nombres( $nombres='', ClienteRepository $clienteRepository): Response
    {
        $data= $clienteRepository->getNombres($nombres);

        return new JsonResponse(  $data );
    }

    /**
     * @Route("/api_v3/informe", name="app_api_cliente_nombre_informe" , methods={"GET"})
     */
    public function index_v3_informe( Request $request, ClienteRepository $clienteRepository): Response
    {
        $nombre_o_dui = $request->query->get('nombre_o_dui');
        $data= $clienteRepository->getInforme($nombre_o_dui);

        return new JsonResponse(  $data );
    }


    

}
