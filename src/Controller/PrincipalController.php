<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PrincipalController extends AbstractController
{
    /**
     * @Route("/prinddssdsdcipal", name="app_principal")
     */
    public function index(): Response
    {
        return $this->render('principal/index.html.twig', [
            'controller_name' => 'PrincipalController',
        ]);
    }

    /**
     * @Route("/", name="app_principal_v2")
     */
    public function indexv2(): Response
    {
        return $this->render('principal/index_v2.html.twig', [
            'controller_name' => 'PrincipalController',
        ]);
    }
}
