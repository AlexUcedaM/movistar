<?php

namespace App\Repository;

use App\Entity\Cliente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Cliente>
 *
 * @method Cliente|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cliente|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cliente[]    findAll()
 * @method Cliente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClienteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cliente::class);
    }

    public function add(Cliente $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Cliente $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getListaClientes( $page, $nombre_o_dui ) // filtra por fechas y por estado menores al que se manda como 3er parametro
    {
        $page= ($page-1)*9;
        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare("
        select A.*, case when count(dr.id)=0 then '[]' else jsonb_agg(dr)::VARCHAR  end as direcciones, A.activo  from (
            select 
                c.primer_nombre as primernombre, c.segundo_nombre as segundonombre, c.primer_apellido as primerapellido, c.segundo_apellido as segundoapellido, c.id, 	 
                    case when count(dp.id)=0 then '[]' else jsonb_agg(dp)::VARCHAR  end as documentos, c.activo
                from cliente c
                    left join	documentos_persona dp on dp.cliente_id = c.id
                    left join documento d on d.id = dp.id_documento_id
                where 
                primer_nombre like :aa or segundo_nombre like :bb or primer_apellido like :cc or segundo_nombre like :dd 
                or concat(primer_nombre, ' ', segundo_nombre, ' ', primer_apellido , ' ',segundo_apellido ) like :ee
                or concat(primer_apellido , ' ',segundo_apellido,' ', primer_nombre, ' ', segundo_nombre ) like :ff
                or dp.numero like :gg 
                    group by c.primer_nombre , c.segundo_nombre, c.primer_apellido, c.segundo_apellido, c.id,  c.activo
        ) A left join 
        direccion dr on dr.cliente_id = A.id
        group by A.primernombre , A.segundonombre, A.primerapellido,A.segundoapellido, A.id, A.documentos, A.activo
        order by A.id DESC
        LIMIT 9
        OFFSET :page;
        ");
        
        $question = "%$nombre_o_dui%";
        $result = $statement->executeQuery(Array(
            'page'=>$page,
            'aa'=> $question,
            'bb'=> $question,
            'cc'=> $question,
            'dd'=> $question,
            'ee'=> $question,
            'ff'=> $question,
            'gg'=> $question,
            
        )); //
        return $result->fetchAll();
    }

    public function getNombres( $nombre ) // filtra por fechas y por estado menores al que se manda como 3er parametro
    {
        
        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare("
            select concat(primer_nombre, ' ', segundo_nombre, ' ', primer_apellido , ' ',segundo_apellido ) as nombres from cliente c 
                where 
                    primer_nombre like :aa or segundo_nombre like :bb or primer_apellido like :cc or segundo_nombre like :dd 
                    or concat(primer_nombre, ' ', segundo_nombre, ' ', primer_apellido , ' ',segundo_apellido ) like :ee
                    or concat(primer_apellido , ' ',segundo_apellido,' ', primer_nombre, ' ', segundo_nombre ) like :ff
            union
            select dp.numero as nombres  from documentos_persona dp 
                where dp.numero like :gg
        ");
        $question = "%$nombre%";
        $result = $statement->executeQuery(Array(
            'aa'=> $question,
            'bb'=> $question,
            'cc'=> $question,
            'dd'=> $question,
            'ee'=> $question,
            'ff'=> $question,
            'gg'=> $question,
        )); 
        return $result->fetchAll();
    }

    public function getInforme( $nombre ) // filtra por fechas y por estado menores al que se manda como 3er parametro
    {
        
        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare("
            select A.id,A.primer_nombre,A.segundo_nombre,A.primer_apellido,A.segundo_apellido,string_agg(A.dui,' ' ) as dui,
                string_agg(A.nit,' ' ) as nit,string_agg(A.nit, ' ') as iss, string_agg(A.email, ' ' ) as correos,string_agg(A.direccion, ' ')  as direccion
                from(
                    select 
                        c.id, 
                        c.primer_nombre, 
                        c.segundo_nombre, 
                        c.primer_apellido ,
                        c.segundo_apellido, 
                        dp_dui.numero as dui, 
                        dp_nit.numero  as nit, 
                        dp_iss.numero  as isss,
                        co.email,
                        concat(de.nombre,' ',mu.nombre,' ', di.descripcion) as direccion
                        from cliente c
                        left join documentos_persona dp_dui on dp_dui.cliente_id = c.id and dp_dui.id_documento_id = 1
                        left join documentos_persona dp_nit on dp_nit.cliente_id = c.id and dp_nit.id_documento_id = 2
                        left join documentos_persona dp_iss on dp_iss.cliente_id = c.id and dp_nit.id_documento_id = 3
                        left join correo co on co.id_cliente_id = c.id
                        left join direccion di on di.cliente_id = c.id
                        left join municipio mu on mu.id = di.municipio_id
                        left join departamento de on de.id = mu.departamento_id 
                        where
                            primer_nombre like :aa or segundo_nombre like :bb or primer_apellido like :cc or segundo_nombre like :dd 
                                or concat(primer_nombre, ' ', segundo_nombre, ' ', primer_apellido , ' ',segundo_apellido ) like :ee
                                or concat(primer_apellido , ' ',segundo_apellido,' ', primer_nombre, ' ', segundo_nombre ) like :ff
                                OR dp_dui.numero like :gg OR  dp_nit.numero like :gg OR  dp_iss.numero like :gg
                ) A
                group by
                A.id,
                A.primer_nombre,
                A.segundo_nombre,
                A.primer_apellido,
                A.segundo_apellido
        ");
        $question = "%$nombre%";
        $result = $statement->executeQuery(Array(
            'aa'=> $question,
            'bb'=> $question,
            'cc'=> $question,
            'dd'=> $question,
            'ee'=> $question,
            'ff'=> $question,
            'gg'=> $question,
        )); 
        return $result->fetchAll();
    }





    
     

//    /**
//     * @return Cliente[] Returns an array of Cliente objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Cliente
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
